import java.io.*;
import narzedzia.*;


public class ManipulacjaDrzewami {
	public static void main(String args[]){

		try{
			int n = args.length;
			DrzewoBST[] drzewa = new DrzewoBST[n];
			for(int i=0; i<n; i++){
				drzewa[i] = new DrzewoBST();
				drzewa[i].setEtykieta(args[i]);
			}

			//pobieranie instrukcji
			BufferedReader we = new BufferedReader(new InputStreamReader(System.in));
			String[] tab;
			int i;
			int j;

			do{
				try{
					System.out.print("komenda: ");
					tab = we.readLine().trim().split("\\s+");
					if(tab[0].equals("insert")){
						i = 0;
						while(!drzewa[i].getEtykieta().equals(tab[1])){i++;}
						drzewa[i].wstaw(new Para(tab[2],Double.parseDouble(tab[3])));
					} else {
					if(tab[0].equals("delete")){
						i = 0;
						while(!drzewa[i].getEtykieta().equals(tab[1])){i++;}
						System.out.println(drzewa[i].usun(tab[2]));
					} else {
					if(tab[0].equals("search")){
						i = 0;
						while(!drzewa[i].getEtykieta().equals(tab[1])){i++;}
						Para p = drzewa[i].szukaj(tab[2]);
						System.out.println(p==null?"no":p.wartosc);
					} else {
					if(tab[0].equals("print")){
						i = 0;
						while(!drzewa[i].getEtykieta().equals(tab[1])){i++;}
						drzewa[i].print();
					} else {
					if(tab[0].equals("print2")){
						i = 0;
						while(!drzewa[i].getEtykieta().equals(tab[1])){i++;}
						drzewa[i].wyswietl();
					} else {
					if(tab[0].equals("clone")){
						i = 0;
						while(!drzewa[i].getEtykieta().equals(tab[1])){i++;}
						j = 0;
						while(!drzewa[j].getEtykieta().equals(tab[2])){j++;}
						drzewa[j] = drzewa[i].clone();
						drzewa[j].setEtykieta(tab[2]);
					} else {
					if(tab[0].equals("exit")){
						System.out.println("bye");
					} else {
						System.out.println("?!? nieznane polecenie !?!");
					}
					}
					}
					}
					}
					}	
					}
				}
			    catch (IOException e) {
			    	tab = new String[1];
			    	tab[0] = new String("quit");
			    	System.err.println("Error: " + e);
			    }
			} while(!tab[0].equals("exit"));


		}
		catch (ArrayIndexOutOfBoundsException ex){
			System.out.println("Zla ilość argumentów!\n");
		}
		catch (IllegalArgumentException ex){
			System.out.println("Błędne argumenty!\n");
		}

	}

}