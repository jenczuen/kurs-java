package narzedzia;

public interface ISlownik{
	Para szukaj(String klucz);
	boolean wstaw(Para p);
	boolean usun(String klucz);
	int ile();
}