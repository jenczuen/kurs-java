package narzedzia;

public class Para implements Comparable<Para>{
	public final String klucz;
	public final Double wartosc;

	public Para(String kl, Double war){
		klucz = kl;
		wartosc = war;
	}

	public int compareTo(Para obiekt) throws IllegalArgumentException{
		if (this.getClass() == obiekt.getClass()) {
			return klucz.compareTo(obiekt.klucz);
		}
		else {
			throw new IllegalArgumentException();
		}
	}
}