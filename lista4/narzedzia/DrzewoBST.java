package narzedzia;

public class DrzewoBST implements ISlownik, Cloneable {
	private Wezel root;
	private String etykieta;

	public DrzewoBST(){
		root = null;
	}

	public void setEtykieta(String e){
		etykieta = new String(e);
	}

	public String getEtykieta(){
		return etykieta;
	}

	public String to_string(){
		return root.to_string();
	}

	public void wyswietl(){
		System.out.println(this.to_string());
	}

	public void print(){
		Wezel start = this.min();
		if(start==null){
			System.out.print("Drzewo jest puste\n");	
		} else {
			String s;
			s = "{ ";
			s += start.wartosc_wezla.klucz;
			s += ", ";
			s += start.wartosc_wezla.wartosc;
			s += "}, ";
			System.out.print(s);
			Wezel tmp = start.succ();
			while(tmp!=null){
				s = "{ ";
				s += tmp.wartosc_wezla.klucz;
				s += ", ";
				s += tmp.wartosc_wezla.wartosc;
				s += "}, ";
				System.out.print(s);
				tmp = tmp.succ();
			}
			System.out.println();
		}
	}

	private Wezel min(){
		if(root == null) return null;
		Wezel tmp = root;
		while(tmp.dziecko_lewe != null){
			tmp = tmp.dziecko_lewe;
		}
		return tmp;
	}

	public boolean wstaw(Para p){
		if(root == null) root = new Wezel(p);
		else root.wstaw(p);

		return true;
	}

	public Para szukaj(String klucz){
		if(root==null) return null;
		Wezel wynik = root.szukaj(klucz);
		return wynik==null ? null : wynik.wartosc_wezla;
	}

	//funkcja zwraca nastepnik elementu o zadanym kluczu
	public Para succ(String klucz){
		Wezel start = root.szukaj(klucz);
		Wezel succ = start.succ();
		return succ==null ? null : succ.wartosc_wezla;
	}

	public boolean usun(String klucz){
		//szukamy wezla do usuniecia
		Wezel usuwany = root.szukaj(klucz);
		if(usuwany == null) return false;

		//jezeli usuwany to root
		//sprawdzamy czy ma dzieci jezeli nie usuwamy go i koniec
		if(root.dziecko_lewe == null || root.dziecko_prawe == null){
			root = null;
		} else {
			//jezeli ma to wykorzystujemy usun z wezla
			Wezel new_root;
			new_root = usuwany.usun();
			if(new_root!=null) root = new_root;
		}

		return true;
	}

	public int ile(){
		if(root == null) return 0;
		else return (root.ile_potomkow()+1);
	}

	public DrzewoBST clone(){
		if(root == null) return null;
		else {
			DrzewoBST klon = new DrzewoBST();
			klon.root = root.clone_with_ancestors();
			return klon;
		}

	}

	private class Wezel{
		public Para wartosc_wezla;
		public Wezel dziecko_lewe;
		public Wezel dziecko_prawe;		
		public Wezel ojciec;

		public Wezel(String klucz, Double wartosc){
			wartosc_wezla = new Para(klucz,wartosc);
		}

		public Wezel(Para p){
			wartosc_wezla = p;
		}

		public void wstaw(Para p){
			if(p.compareTo(wartosc_wezla) <= 0) {
				if(dziecko_lewe == null){
					dziecko_lewe = new Wezel(p);
					dziecko_lewe.ojciec = this;
				} else {
					dziecko_lewe.wstaw(p);
				}
			} else {
				if(dziecko_prawe == null){
					dziecko_prawe = new Wezel(p);
					dziecko_prawe.ojciec = this;					
				} else {
					dziecko_prawe.wstaw(p);
				}
			}
		}

		public Wezel szukaj(String klucz){
			if (klucz.compareTo(wartosc_wezla.klucz) == 0) {
				return this;
			} else {
				if(klucz.compareTo(wartosc_wezla.klucz) < 0) {
					if(dziecko_lewe == null){
						return null;
					} else {
						return dziecko_lewe.szukaj(klucz);
					}
				} else {
					if(dziecko_prawe == null){
						return null;
					} else {
						return dziecko_prawe.szukaj(klucz);
					}
				}
			}
		}

		//funkcja zwraca nastepnik wezla
		public Wezel succ(){
			if(this.dziecko_prawe == null){
				if(this.ojciec == null) return null;
				else {
					if(this.ojciec.dziecko_lewe == this) return this.ojciec;

					Wezel tmp = this;
					while(tmp.ojciec != null){
						if(tmp.ojciec.dziecko_lewe == tmp) return tmp.ojciec;
						else tmp = tmp.ojciec;
					}
					return null;
				}
			} else {
				if(dziecko_prawe.dziecko_lewe == null){
					return dziecko_prawe;
				} else {
					Wezel tmp;
					tmp = dziecko_prawe.dziecko_lewe;
					while(tmp.dziecko_lewe != null){
						tmp = tmp.dziecko_lewe;
					}
					return tmp;
				}

			}
		}

		//usuwa wezel i zwraza nowego roota jezeli root sie zmienil
		//lub null jezeli sie nie zmienil
		public Wezel usun(){

			boolean root=false;
			if(this.ojciec == null) root=true;
			
			boolean jest_lewym_dzieckiem = false;			
			if(!root){
				if(this.ojciec.dziecko_lewe == this) jest_lewym_dzieckiem = true;
			}

			if(this.dziecko_lewe == null && this.dziecko_prawe == null){
				//brak dzieci, zakladam ze w drzewie jest conajmniej jeden wezel
				//wiec nie rozwazam przypadku usuwania roota(dzieje sie to poziom wyzej)
				if(jest_lewym_dzieckiem) this.ojciec.dziecko_lewe = null;
				else this.ojciec.dziecko_prawe = null;					
				this.ojciec = null;		
				return null;		
			} else {
				//one child
				if(this.dziecko_lewe != null && this.dziecko_prawe == null){
					if(!root){
						this.dziecko_lewe.ojciec = this.ojciec;
						if(jest_lewym_dzieckiem) this.ojciec.dziecko_lewe = this.dziecko_lewe;
						else this.dziecko_prawe = this.dziecko_lewe;					
						return null;
					} else {
						this.dziecko_lewe.ojciec = this.ojciec;
						return this.dziecko_lewe;
					}
				} else {
					//another one child
					if(this.dziecko_lewe == null && this.dziecko_prawe != null){
						if(!root){
							this.dziecko_prawe.ojciec = this.ojciec;
							if(jest_lewym_dzieckiem) this.ojciec.dziecko_lewe = this.dziecko_prawe;
							else this.ojciec.dziecko_prawe = this.dziecko_prawe;							
							return null;
						} else {
							this.dziecko_prawe.ojciec = this.ojciec;
							return this.dziecko_prawe;
						}
					} else {
						//two children
						Wezel succ = this.succ();
						Wezel succ_clone = succ.clone();

						succ_clone.dziecko_lewe = this.dziecko_lewe;
						succ_clone.dziecko_prawe = this.dziecko_prawe;
						succ_clone.ojciec = this.ojciec;

						this.dziecko_prawe.ojciec = succ_clone;
						this.dziecko_lewe.ojciec = succ_clone;
	
						if(!root){
							if(jest_lewym_dzieckiem) this.ojciec.dziecko_lewe = succ_clone;
							else this.ojciec.dziecko_prawe = succ_clone;
							succ.usun();
							return null;
						} else {
							succ.usun();
							return succ_clone;
						}

						/*
						String s = "Usuwam wezel o kluczu: ";
						s+= this.wartosc_wezla.klucz;
						s+= "\nJego ojcem jest: ";
						s+= this.ojciec.wartosc_wezla.klucz;
						s+= "\nWezel jest zastepowany swoim sukcesorem: ";
						s+= succ_clone.wartosc_wezla.klucz;
						s+= "\nKtory ma teraz dziecko lewe: ";
						s+= succ_clone.dziecko_lewe==null ? "brak" : succ_clone.dziecko_lewe.wartosc_wezla.klucz;
						s+= "\nOraz dziecko prawe: ";						
						s+= succ_clone.dziecko_prawe==null ? "brak" : succ_clone.dziecko_prawe.wartosc_wezla.klucz;	
						s+= "\n";
						System.out.println(s);
						*/
					}
				}
			}
		}

		public Wezel clone(){
			return new Wezel(new String(wartosc_wezla.klucz),new Double(wartosc_wezla.wartosc));
		}

		public Wezel clone_with_ancestors(){
			Wezel klon;
			klon = this.clone();
			if(this.dziecko_lewe!=null){
				klon.dziecko_lewe = this.dziecko_lewe.clone_with_ancestors();
				klon.dziecko_lewe.ojciec = klon;
			}
			if(this.dziecko_prawe!=null){
				klon.dziecko_prawe = this.dziecko_prawe.clone_with_ancestors();
				klon.dziecko_prawe.ojciec = klon;				
			}
			return klon;
		}

		public int ile_potomkow(){
			int ile = 0;
			if(this.dziecko_lewe!=null){
				ile += 1;
				ile += this.dziecko_lewe.ile_potomkow();
			}
			if(this.dziecko_prawe!=null){
				ile += 1;
				ile += this.dziecko_prawe.ile_potomkow();
			}
			return ile;
		}

		public String to_string(){
			String s = "";
			s += "Para:\n*Klucz ojca = ";
			s += this.ojciec == null ? "root" : this.ojciec.wartosc_wezla.klucz;
			s += "*\n**Klucz = ";
			s += wartosc_wezla.klucz;
			s += "**\n**Wartosc = ";
			s += wartosc_wezla.wartosc;
			s += "**\nDziecko lewe:\n";
			s += dziecko_lewe == null ? "puste" : dziecko_lewe.to_string();
			s += "-------------\n";
			s += "Dziecko prawe:\n";
			s += dziecko_prawe == null ? "puste" : dziecko_prawe.to_string();
			s += "-------------\n";
			s += "=============\n";			
			return s;
		}
	}
}