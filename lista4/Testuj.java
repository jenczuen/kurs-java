import java.util.Scanner;
import testy.*;

public class Testuj {
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		(new TestPara()).wykonaj();
		scanner.nextLine();
		(new TestDrzewoBST()).wykonaj();
		scanner.nextLine();
	}
}