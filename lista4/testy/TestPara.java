package testy;

import narzedzia.Para;

public class TestPara extends Test {
	public void wykonaj(){
		System.out.println("Test Para:");

		Para p1 = new Para("a",100.0);
		Para p2 = new Para("aa",100.0);
		Para p3 = new Para("aaa",100.0);

		System.out.println(p1.klucz+" cmp "+p2.klucz+" :"+p1.compareTo(p2));
		System.out.println(p2.klucz+" cmp "+p1.klucz+" :"+p2.compareTo(p1));		
		System.out.println(p3.klucz+" cmp "+p3.klucz+" :"+p3.compareTo(p3));				
		System.out.println(p1.klucz+" cmp "+p3.klucz+" :"+p1.compareTo(p3));
		System.out.println(p2.klucz+" cmp "+p3.klucz+" :"+p2.compareTo(p3));
	}
}