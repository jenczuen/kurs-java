package testy;

import narzedzia.*;

public class TestDrzewoBST extends Test {
	public void wykonaj(){
		System.out.println("Test DrzewoBST:");
		DrzewoBST d = new DrzewoBST();
		Para p;
		String dane = "dbfacegh";


		//				  d
		//               / \
		//              /   \
		//             /     \
		//            b       f
		//           / \     / \
		//          a   c   e   g
		//                       \
		//                        h
 
 		for(int i=0;i<dane.length();i++){
  			d.wstaw(new Para(Character.toString(dane.charAt(i)),1.0));
 		}

		d.wyswietl();

 		for(int i=0;i<dane.length();i++){
			p = d.succ(Character.toString(dane.charAt(i)));
			System.out.println("Nastepnik "+dane.charAt(i)+": "+(p==null?"nie ma":p.klucz));
 		}

		System.out.println(d.ile());

 		//test usuwania
 		d.usun("d");

		d.wyswietl();

		System.out.println(d.ile());


		DrzewoBST dd = d.clone();
		dd.wyswietl();

 		//test szukania
		/*
		System.out.println("Wynik szukania: ");
		p = d.szukaj("e");
		if (p==null){
			System.out.println("Pucha!");	
		} else {
			System.out.println("Klucz: "+p.klucz);		
			System.out.println("Wartosc: "+p.wartosc);							
		}
		*/
	}
}