import narzedzia.obliczenia.*;
import narzedzia.kontenery.*;
import java.util.LinkedList;
import java.util.List;

public class TestInstrukcja {
	public static void main(String args[]){
		Zbior zbior = new Zbior();
		Zmienna.zbior = zbior;
		PrzypisanieWartosciZmiennej.zbior = zbior;
		DeklaracjaZmiennej.zbior = zbior;
		Wypisz.zbior = zbior;
		Wczytaj.zbior = zbior;		

		List<Instrukcja> program = new LinkedList<Instrukcja>();

		program.add(new DeklaracjaZmiennej("x"));		
		program.add(new DeklaracjaZmiennej("n"));
		program.add(new Wczytaj("n"));		
		program.add(new InstrukcjaWarunkowa(
						new Mniejsze(
								new Liczba(1),
								new Zmienna("n")
							),
						new PrzypisanieWartosciZmiennej("x",new Liczba(5)),
						new PrzypisanieWartosciZmiennej("x",new Liczba(10))
			));
		program.add(new Wypisz("x"));

		Instrukcja i = new Blok(program);
		System.out.println("Kod:");
		System.out.println(i.to_string());
		System.out.println("Wynik:");
		i.wykonaj();
	}
}