package narzedzia.obliczenia;

public abstract class Wyrazenie{
	public abstract int oblicz();
	public abstract String to_string();	
}