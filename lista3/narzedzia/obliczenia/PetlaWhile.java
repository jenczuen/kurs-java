package narzedzia.obliczenia;

public class PetlaWhile extends Instrukcja {
	private Wyrazenie warunek;
	private Instrukcja instrukcja;

	public PetlaWhile(Wyrazenie w, Instrukcja i){
		warunek = w;
		instrukcja = i;
	}

	public int wykonaj(){
		while(warunek.oblicz() != 0){
			instrukcja.wykonaj();
		}
		return 1;
	}

	public String to_string(){
		String s = "while (";
		s += warunek.to_string()+")\n{\n";
		s += instrukcja.to_string()+"}\n";
		return s;
	}

}