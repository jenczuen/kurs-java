package narzedzia.obliczenia;

public class Wypisz extends InstrukcjaOperujacaNaZmiennej{

	public Wypisz(String kl){
		klucz = kl;
	}

	public int wykonaj(){
		wartosc = zbior.szukaj(klucz);		
		System.out.println(Integer.toString(wartosc));
		return 1;
	}

	public String to_string(){
		return "write "+klucz+"\n";
	}
}