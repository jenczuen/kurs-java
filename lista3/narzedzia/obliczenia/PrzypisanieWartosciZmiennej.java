package narzedzia.obliczenia;

import narzedzia.kontenery.*;

public class PrzypisanieWartosciZmiennej extends InstrukcjaOperujacaNaZmiennej{
	private Wyrazenie wyrazenie;

	public PrzypisanieWartosciZmiennej(String kl, Wyrazenie w){
		klucz = kl;
		wyrazenie = w;
	}

	public int wykonaj(){
		zbior.przypisz(klucz,wyrazenie.oblicz());
		return 1;
	}	

	public String to_string(){
		return klucz+" = "+wyrazenie.to_string()+"\n";
	}

}