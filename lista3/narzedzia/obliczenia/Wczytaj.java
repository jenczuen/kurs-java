package narzedzia.obliczenia;

import narzedzia.kontenery.*;
import java.util.Scanner;

public class Wczytaj extends InstrukcjaOperujacaNaZmiennej{

	public Wczytaj(String kl){
		klucz = kl;
	}

	public int wykonaj(){
		Scanner scanner = new Scanner(System.in);
		System.out.print("Podaj wartosc "+klucz+": ");
		String w = scanner.nextLine();
		zbior.przypisz(klucz,Integer.parseInt(w));
		return 1;
	}

	public String to_string(){
		return "read "+klucz+"\n";
	}
}