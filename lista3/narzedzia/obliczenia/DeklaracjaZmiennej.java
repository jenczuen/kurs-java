package narzedzia.obliczenia;

import narzedzia.kontenery.*;

public class DeklaracjaZmiennej extends InstrukcjaOperujacaNaZmiennej{
	public DeklaracjaZmiennej(String kl){
		klucz = kl;
	}

	public int wykonaj(){
		zbior.wstaw(klucz,0);
		return 1;
	}	

	public String to_string(){
		return "var "+klucz+"\n";
	}
}