package narzedzia.obliczenia;

import java.util.LinkedList;
import java.util.List;
import java.util.*;

public class Blok extends Instrukcja{
	private List<Instrukcja> blok;

	public Blok(List<Instrukcja> b){
		blok = b;
	}

	public int wykonaj(){
		Iterator itr = blok.iterator();
		Instrukcja i;
		while(itr.hasNext()){
			i = (Instrukcja)(itr.next());
			i.wykonaj();
		}
		return 1;
	}

	public String to_string(){
		String s = "";
		Iterator itr = blok.iterator();
		Instrukcja i;
		while(itr.hasNext()){
			i = (Instrukcja)(itr.next());
			s += i.to_string();
		}
		return s;		
	}
}