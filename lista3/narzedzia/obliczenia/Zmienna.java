package narzedzia.obliczenia;

import narzedzia.kontenery.*;

public class Zmienna extends Wyrazenie{
	public static Zbior zbior = new Zbior();
	private String klucz;

	public Zmienna(String kl){
		klucz = kl;
		zbior.wstaw(klucz,0);
	}

	public void ustal(int wartosc){
		zbior.przypisz(klucz,wartosc);
	}

	public int oblicz(){
		return zbior.szukaj(klucz);
	}

	public String to_string(){
		return klucz;
//		return Integer.toString(zbior.szukaj(klucz));
	}
}