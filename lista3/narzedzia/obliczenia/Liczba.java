package narzedzia.obliczenia;

public class Liczba extends Wyrazenie{
	private final int wartosc;

	public Liczba(int w){
		wartosc = w;
	}

	public int oblicz(){
		return wartosc;
	}

	public String to_string(){
		return Integer.toString(wartosc);
	}
}