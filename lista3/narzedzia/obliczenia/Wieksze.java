package narzedzia.obliczenia;

public class Wieksze extends Wyrazenie	{
	protected final Wyrazenie a;
	protected final Wyrazenie b;

	public Wieksze(Wyrazenie a, Wyrazenie b){
		this.a = a;
		this.b = b;
	}	

	public int oblicz(){
		return (a.oblicz() > b.oblicz()) ? 1 : 0;
	}

	public String to_string(){
		return a.to_string()+">"+b.to_string();
	}

}