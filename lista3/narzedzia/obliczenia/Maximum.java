package narzedzia.obliczenia;

public class Maximum extends Wyrazenie	{
	protected final Wyrazenie a;
	protected final Wyrazenie b;

	public Maximum(Wyrazenie a, Wyrazenie b){
		this.a = a;
		this.b = b;
	}	

	public int oblicz(){
		int w_a = a.oblicz();
		int w_b = b.oblicz();
		return (w_a < w_b) ? w_b : w_a;
	}

	public String to_string(){
		return "max("+a.to_string()+","+b.to_string()+")";
	}

}