package narzedzia.obliczenia;

public class Mnozenie extends Wyrazenie	{
	protected final Wyrazenie a;
	protected final Wyrazenie b;

	public Mnozenie(Wyrazenie a, Wyrazenie b){
		this.a = a;
		this.b = b;
	}	

	public int oblicz(){
		return a.oblicz() * b.oblicz();
	}

	public String to_string(){
		return "("+a.to_string()+"*"+b.to_string()+")";
	}

}