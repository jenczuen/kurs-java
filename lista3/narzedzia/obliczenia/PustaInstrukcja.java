package narzedzia.obliczenia;

public class PustaInstrukcja extends Instrukcja{
	public PustaInstrukcja(){}

	public int wykonaj(){
		return 1;
	}

	public String to_string(){
		return "no operation";
	}
}