package narzedzia.obliczenia;

public class PrzeciwnyZnak extends Wyrazenie	{
	protected final Wyrazenie a;

	public PrzeciwnyZnak(Wyrazenie a){
		this.a = a;
	}	

	public int oblicz(){
		return -a.oblicz();
	}

	public String to_string(){
		return "-"+a.to_string();
	}

}