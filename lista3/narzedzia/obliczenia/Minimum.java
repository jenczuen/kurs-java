package narzedzia.obliczenia;

public class Minimum extends Wyrazenie	{
	protected final Wyrazenie a;
	protected final Wyrazenie b;

	public Minimum(Wyrazenie a, Wyrazenie b){
		this.a = a;
		this.b = b;
	}	

	public int oblicz(){
		int w_a = a.oblicz();
		int w_b = b.oblicz();
		return (w_a < w_b) ? w_a : w_b;
	}

	public String to_string(){
		return "min("+a.to_string()+","+b.to_string()+")";
	}

}