package narzedzia.obliczenia;

public class WartoscBezwzgl extends Wyrazenie	{
	protected final Wyrazenie a;

	public WartoscBezwzgl(Wyrazenie a){
		this.a = a;
	}	

	public int oblicz(){
		int w_a = a.oblicz();
		return (w_a >= 0) ? w_a : -w_a;
	}

	public String to_string(){
		return "|"+a.to_string()+"|";
	}

}