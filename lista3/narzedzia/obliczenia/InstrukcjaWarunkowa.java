package narzedzia.obliczenia;

public class InstrukcjaWarunkowa extends Instrukcja {
	private Wyrazenie warunek;
	private Instrukcja prawda;
	private Instrukcja falsz;	

	public InstrukcjaWarunkowa(Wyrazenie w, Instrukcja p, Instrukcja f){
		warunek = w;
		prawda = p;
		falsz = f;
	}

	public int wykonaj(){
		if (warunek.oblicz() != 0){			
			prawda.wykonaj();
		}
		else {
			falsz.wykonaj();
		}
		return 1;
	}

	public String to_string(){
		String s = "if (";
		s += warunek.to_string()+")\n{\n";
		s += prawda.to_string()+"\n}\nelse\n{\n";
		s += falsz.to_string()+"\n}\n";
		return s;
	}

}