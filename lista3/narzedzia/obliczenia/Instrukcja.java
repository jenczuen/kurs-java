package narzedzia.obliczenia;

public abstract class Instrukcja {
	public abstract int wykonaj();	
	public abstract String to_string();		
}