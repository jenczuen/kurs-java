import java.util.LinkedList;
import java.util.List;
import java.util.*;

public class Zbior{
	private List<Para> list;

	public Zbior(){
		list = new LinkedList<Para>();
	}

	public void wstaw(String kl, int wart) throws IllegalArgumentException{
		list.add(new Para(kl,wart));			
	}
	
	public int szukaj(String kl) throws IllegalArgumentException{
		Iterator itr = list.iterator();
		Para p;
		try{
			while(itr.hasNext()){
				p = (Para)(itr.next());
				if( p.klucz.equals(kl) ){
					return p.getWartosc();
				}
			}
			throw new IllegalArgumentException();
		}
		catch (IllegalArgumentException ex){
			throw new IllegalArgumentException();	
		}
	}

	public int ile(){
		return list.size();
	}

	public void czysc(){
		list.clear();
	}
}