import java.util.Scanner;
import testy.*;

public class Testy{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		(new TestInstrukcja()).testuj();
		scanner.nextLine();
		(new TestOperacjeNaZmiennych()).testuj();
		scanner.nextLine();
		(new TestPetla()).testuj();
		scanner.nextLine();
		(new TestWyrazenie()).testuj();
		scanner.nextLine();
		(new TestZbior()).testuj();
	}

}