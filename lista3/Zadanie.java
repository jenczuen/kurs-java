import narzedzia.obliczenia.*;
import narzedzia.kontenery.*;
import java.util.LinkedList;
import java.util.List;

public class Zadanie {
	public static void main(String args[]){
		Zbior zbior = new Zbior();
		Zmienna.zbior = zbior;
		PrzypisanieWartosciZmiennej.zbior = zbior;
		DeklaracjaZmiennej.zbior = zbior;
		Wypisz.zbior = zbior;
		Wczytaj.zbior = zbior;		

		List<Instrukcja> program = new LinkedList<Instrukcja>();
		program.add(new DeklaracjaZmiennej("silnia"));
		program.add(new PrzypisanieWartosciZmiennej("silnia",new Liczba(1)));
		program.add(new DeklaracjaZmiennej("n"));
		program.add(new Wczytaj("n"));		

			List<Instrukcja> blok_prawda = new LinkedList<Instrukcja>();
			blok_prawda.add(new DeklaracjaZmiennej("i"));
			blok_prawda.add(new PrzypisanieWartosciZmiennej("i",new Liczba(2)));
				List<Instrukcja> blok_petla = new LinkedList<Instrukcja>();
				blok_petla.add(new PrzypisanieWartosciZmiennej("silnia",new Mnozenie(
																	new Zmienna("silnia"),
																	new Zmienna("i")
																)));
				blok_petla.add(new PrzypisanieWartosciZmiennej("i",new Dodawanie(
																	new Zmienna("i"),
																	new Liczba(1)
																)));
			blok_prawda.add(new PetlaWhile(new MniejszeRowne(
													new Zmienna("i"),
													new Zmienna("n")
												),
											new Blok(blok_petla)
											));

		program.add(new InstrukcjaWarunkowa(
						new Mniejsze(
								new Liczba(1),
								new Zmienna("n")
							),
						new Blok(blok_prawda),
						new PustaInstrukcja()
			));
		program.add(new Wypisz("silnia"));

		Instrukcja i = new Blok(program);
		System.out.println("Kod:");
		System.out.println(i.to_string());
		System.out.println("Wynik:");
		i.wykonaj();
	}
}