package testy;

import narzedzia.obliczenia.*;
import narzedzia.kontenery.*;
import java.util.LinkedList;
import java.util.List;

public class TestPetla extends Test{
	public void testuj(){
		System.out.println("--------------- Test Petla");

		Zbior zbior = new Zbior();
		InstrukcjaOperujacaNaZmiennej.zbior = zbior;				
		Zmienna.zbior = zbior;

		List<Instrukcja> program = new LinkedList<Instrukcja>();
		program.add(new DeklaracjaZmiennej("i"));
		program.add(new PrzypisanieWartosciZmiennej("i",new Liczba(2)));
			List<Instrukcja> blok_petla = new LinkedList<Instrukcja>();
			blok_petla.add(new Wypisz("i"));
			blok_petla.add(new PrzypisanieWartosciZmiennej("i",new Dodawanie(
																	new Zmienna("i"),
																	new Liczba(1)
																)));
			blok_petla.add(new Wypisz("i"));
		program.add(new PetlaWhile(
						new Mniejsze(
								new Zmienna("i"),
								new Liczba(10)
							),
						new Blok(blok_petla)
					));
		program.add(new Wypisz("i"));

		Instrukcja i = new Blok(program);
		System.out.println("Kod:");
		System.out.println(i.to_string());
		System.out.println("Wynik:");
		i.wykonaj();
		System.out.println("================= koniec Test Petla");		
	}
}