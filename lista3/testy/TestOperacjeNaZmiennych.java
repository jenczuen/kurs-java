package testy;

import narzedzia.obliczenia.*;
import narzedzia.kontenery.*;
import java.util.LinkedList;
import java.util.List;

public class TestOperacjeNaZmiennych extends Test{
	public void testuj(){
		System.out.println("--------------- Test Operacja na zmiennych");		
		Zbior zbior = new Zbior();

		InstrukcjaOperujacaNaZmiennej.zbior = zbior;
		Zmienna.zbior = zbior;

		List<Instrukcja> program = new LinkedList<Instrukcja>();

		program.add(new DeklaracjaZmiennej("x"));
		program.add(new Wypisz("x"));
		program.add(new PrzypisanieWartosciZmiennej("x",new Liczba(10)));
		program.add(new Wypisz("x"));

		Instrukcja i = new Blok(program);
		System.out.println("Kod:");
		System.out.println(i.to_string());
		System.out.println("Wynik:");
		i.wykonaj();
		System.out.println("================= koniec Test Operacje na zmiennych");		
	}
}