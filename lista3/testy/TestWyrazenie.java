package testy;

import narzedzia.obliczenia.*;
import narzedzia.kontenery.*;

public class TestWyrazenie extends Test{
	public void testuj(){
		System.out.println("--------------- Test Wyrazenie");		
		Wyrazenie w;
		Zbior zbior = new Zbior();
		Zmienna.zbior = zbior;
		Zmienna x = new Zmienna("x");
		x.ustal(-3);


		System.out.println("Wartosc x: "+(new Zmienna("x")).oblicz());		
		//3+5
		w = new Dodawanie(
				new Liczba(3),
				new Liczba(5)
			);
		System.out.println(w.oblicz());
		System.out.println(w.to_string());		

		//2+x*7
		w = new Dodawanie(
				new Liczba(2),
				new Mnozenie(
					new Zmienna("x"),
					new Liczba(7)
				)
			);
		System.out.println(w.oblicz());
		System.out.println(w.to_string());		

		//(3*11-1)/(7*5)
		w = new Dzielenie(
				new Odejmowanie(
						new Mnozenie(
								new Liczba(3),
								new Liczba(11)
							),
						new Liczba(1)
					),
				new Dodawanie(
						new Liczba(7),
						new Liczba(5)
					)
			);
		System.out.println(w.oblicz());
		System.out.println(w.to_string());		

		//((x+13)*x)/2
		w = new Dzielenie(
				new Mnozenie(
						new Dodawanie(
								new Zmienna("x"),
								new Liczba(13)
							),
						new Zmienna("x")
					),
				new Liczba(2)
			);
		System.out.println(w.oblicz());
		System.out.println(w.to_string());		

		//17*x+19<0
		w = new Mniejsze(
				new Dodawanie(
						new Mnozenie(
								new Liczba(17),
								new Zmienna("x")
							),
						new Liczba(19)
					),
				new Liczba(0)
			);
		System.out.println(w.oblicz());
		System.out.println(w.to_string());		

		System.out.println("================= koniec Test Wyrazenie");		
	}
}