package testy;

import narzedzia.obliczenia.*;
import narzedzia.kontenery.*;
import java.util.LinkedList;
import java.util.List;

public class TestInstrukcja extends Test{
	public void testuj(){
		System.out.println("--------------- Test Instrukcja");
		Zbior zbior = new Zbior();
		InstrukcjaOperujacaNaZmiennej.zbior = zbior;		
		Zmienna.zbior = zbior;

		List<Instrukcja> program = new LinkedList<Instrukcja>();

		program.add(new DeklaracjaZmiennej("x"));		
		program.add(new DeklaracjaZmiennej("n"));
		program.add(new Wczytaj("n"));		
		program.add(new InstrukcjaWarunkowa(
						new Mniejsze(
								new Liczba(1),
								new Zmienna("n")
							),
						new PrzypisanieWartosciZmiennej("x",new Liczba(5)),
						new PrzypisanieWartosciZmiennej("x",new Liczba(10))
			));
		program.add(new Wypisz("x"));

		Instrukcja i = new Blok(program);
		System.out.println("Kod:");
		System.out.println(i.to_string());
		System.out.println("Wynik:");
		i.wykonaj();
		System.out.println("================= koniec Test Instrukcja");				
	}
}