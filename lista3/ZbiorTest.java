import narzedzia.kontenery.*;

public class ZbiorTest{
	public static void main(String[] args){
		Zbior z = new Zbior();

		z.wstaw("w1",1);
		z.wstaw("w2",2);
		z.wstaw("w3",3);
		System.out.println(z.ile());
		System.out.println("w3 => "+z.szukaj("w2"));
		z.przypisz("w2",100);
		System.out.println("w3 => "+z.szukaj("w2"));		
		z.czysc();
		System.out.println(z.ile());		
	}
}