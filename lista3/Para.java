public class Para{
	public final String klucz;
	private int wartosc;

	public Para(String k){
		klucz = k;
	}

	public Para(String k, int w){
		klucz = k;
		wartosc = w;
	}

	public void setWartosc(int w){
		wartosc = w;
	}

	public int getWartosc(){
		return wartosc;
	}

}	