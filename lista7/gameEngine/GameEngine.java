package gameEngine;

import gui.*;
import javax.swing.*;
import java.util.*;

public class GameEngine {

	Interface gameInterface;
	int size;

	public GameEngine(){
	}

	public void start(){
		gameInterface = new Interface(this);
		gameInterface.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameInterface.setSize(700,700);
		gameInterface.setVisible(true);

		size = Interface.getBoardSize();

		gameInterface.setInformationLabelContent("Ruch czarnych...");

	}

	public void newGame(){
		gameInterface.clearBoard();
		gameInterface.setInformationLabelContent("Nowa gra. Ruch czarnych...");
	}

	public void blackOnField(int x, int y){
		gameInterface.setInformationLabelContent("Czarny na polu: "+x+","+y+". "+"Ruch bialych...");
		gameInterface.changePlayer();

		Random genX = new Random(x);
		Random genY = new Random(y);
		int newX, newY;
		do{
			newX = genX.nextInt(size)+1;
			newY = genY.nextInt(size)+1;
		} while (!gameInterface.putPawn(newX,newY));

		gameInterface.changePlayer();
		gameInterface.setInformationLabelContent("Bialy na polu: "+newX+","+newY+". "+"Ruch czarnych...");
	}

}