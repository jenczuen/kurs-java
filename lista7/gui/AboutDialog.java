package gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AboutDialog extends Dialog{
    private Panel panel;
    private Label label;
    private Button btn;

    private WindowListener frameList = new WindowAdapter(){
        public void windowClosing(WindowEvent ev){
            AboutDialog.this.hide();
        } 
    };
        
    private ActionListener btnList = new ActionListener(){
        public void actionPerformed (ActionEvent ev){       
            AboutDialog.this.hide();
        }
    };
        
    public AboutDialog(Frame owner){
        super(owner, "About the program");
        this.setSize(100,100);
        this.setModal(true);
        this.addWindowListener(frameList);
        panel = new Panel();
        label = new Label("Made by Jedrek.");
        panel.add(label);
        btn = new Button("OK");
        panel.add(btn);
        btn.addActionListener(btnList);
        this.add(panel);

        panel.setLayout(new FlowLayout() );
    }
}
