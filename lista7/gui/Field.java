package gui;

import java.awt.*;
import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import gameEngine.*;

public class Field extends JPanel{
  private static Boolean blacksTurn = true;
  private Boolean itHasBlackPawn;
  private Boolean pawnShown = false;
  private Boolean crossed = false;
  private int crossingDir;
  private int x,y;
  private GameEngine gameEngine;

  public Field(int x, int y, GameEngine gameEngine){
    super();
    this.addMouseListener(new FieldMouseListener());
    this.gameEngine = gameEngine;
    this.x = x;
    this.y = y;
  }

  public void paintComponent(Graphics g){
    int x = this.getWidth();
    int y = this.getHeight();
    g.setColor(Color.black);
    g.drawRect(0,0,x,y);
    g.setColor(Color.blue);    
    g.fillRect(1,1,x-1,y-1);
    if(pawnShown){
      g.setColor(Color.black);
      g.drawOval(2,2,x-4,y-4);
      
      if(itHasBlackPawn) g.setColor(Color.black); 
      else g.setColor(Color.white); 
      g.fillOval(3,3,x-5,y-5);

      if(crossed){
        g.setColor(Color.red);
        switch(crossingDir){
          case 1:
            g.fillRect(0,y/2,x,2);
            break;
          case 2:
            g.fillRect(x/2,0,2,y);
            break;
          case 3:
            g.drawLine(0,0,x,y);
            g.drawLine(0,1,x-1,y);
            break;
        }
      }
    }
  }

  public Boolean isPawnShown(){
    return pawnShown;
  }

  public Boolean showPawn(){
    if(!pawnShown){
      pawnShown = true;
      itHasBlackPawn = blacksTurn;    
      repaint();
      if(blacksTurn) gameEngine.blackOnField(x,y);
      return true;
    } else return false;
  }

  public Boolean hidePawn(){
    if(pawnShown){
      pawnShown = false;
      repaint();
      return true;
    } else return false;
  }

  public Boolean cross(int direction){
    if(pawnShown){
      crossed = true;
      crossingDir = direction;
      repaint();
      return true;
    } else return false;
  }

  public static void changePlayer(){
    blacksTurn = !blacksTurn;
  }

}