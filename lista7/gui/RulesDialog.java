package gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class RulesDialog extends Dialog{
    private Panel panel;
    private Label label;
    private Button btn;

    private WindowListener frameList = new WindowAdapter(){
        public void windowClosing(WindowEvent ev){
            RulesDialog.this.hide();
        } 
    };
        
    private ActionListener btnList = new ActionListener(){
        public void actionPerformed (ActionEvent ev){       
            RulesDialog.this.hide();
        }
    };
        
    public RulesDialog(Frame owner){
        super(owner, "Rules of the game");
        this.setSize(100,100);
        this.setModal(true);
        this.addWindowListener(frameList);
        panel = new Panel();
        label = new Label("Rules of the game......\n.....");
        panel.add(label);
        btn = new Button("OK");
        panel.add(btn);
        btn.addActionListener(btnList);
        this.add(panel);

        panel.setLayout(new FlowLayout() );
    }
}
