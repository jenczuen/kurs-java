package gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import gameEngine.*;
 
public class Interface extends JFrame implements ActionListener {

	private JMenu gameMenu;
	private JMenu propertiesMenu;
	private JMenu helpMenu;

	private JTextArea textArea;
	private JLabel informationLabel;

	private GameEngine gameEngine;

	private JPanel[][] pola;

	private static int size;
 
    public Interface(GameEngine gameEngine) {
		super("Gmoku");

		this.gameEngine = gameEngine;
		this.size = 19;

		JPanel okno = new JPanel(new BorderLayout());

		okno.setBorder(BorderFactory.createLineBorder(Color.black, 5));

		createGameMenu();
		createPropertiesMenu();
		createHelpMenu();
	
		JMenuBar bar = new JMenuBar();
		okno.add(bar, BorderLayout.PAGE_START);
		bar.add(gameMenu);
		bar.add(propertiesMenu);
		bar.add(helpMenu);

		pola = new JPanel[size+2][size+2];

		JPanel plansza = new JPanel();
		plansza.setBackground(Color.white);
		plansza.setLayout(new GridLayout(size+2,size+2));
		okno.add(plansza, BorderLayout.CENTER);

		for(int i=0; i<=size+1; i++){
			for(int j=0; j<=size+1; j++){
				if(i!=0 && j!=0 && i!=size+1 && j!=size+1){
					pola[i][j] = new Field(i,j,gameEngine);
				} else {
					pola[i][j] = new DescriptionField(i,j,size+2);
				}
				plansza.add(pola[i][j]);
			}
		}

		JPanel labelPanel = new JPanel(new FlowLayout());
		informationLabel = new JLabel("Info");
		labelPanel.add(informationLabel);
		okno.add(labelPanel, BorderLayout.PAGE_END);
		this.add(okno);
    }

    public void clearBoard(){
		for(int i=1; i<=size; i++){
			for(int j=1; j<=size; j++){
				((Field)pola[i][j]).hidePawn();
			}
		}
    }

    public static int getBoardSize(){
    	return size;
    }

    public void setInformationLabelContent(String content){
    	informationLabel.setText(content);
    }

    public Boolean putPawn(int x, int y){
    	if(0 < x && x <= size && 0 < y && y <= size) 
    		return ((Field)pola[x][y]).showPawn();
    	else return false;
    }

    public void changePlayer(){
    	Field.changePlayer();
    }

    private void createGameMenu() {
		gameMenu = new JMenu("Game");
		gameMenu.setMnemonic('G');
	
		JMenuItem newItem = new JMenuItem("New");
		newItem.setMnemonic('A');
		newItem.setEnabled(true);
		newItem.addActionListener(this);
		gameMenu.add(newItem);
		gameMenu.addSeparator();

		newItem = new JMenuItem("Exit");
		newItem.setMnemonic('x');
		newItem.setEnabled(true);
		newItem.addActionListener(this);
		gameMenu.add(newItem);
	}
	
    private void createPropertiesMenu() {
		propertiesMenu = new JMenu("Properties");
		propertiesMenu.setMnemonic('P');
        
		JMenuItem newItem = new JMenuItem("Oznaczenie pol");
		newItem.setMnemonic('O');
		newItem.setEnabled(true);
		newItem.addActionListener(this);
		propertiesMenu.add(newItem);

		newItem = new JMenuItem("Skreslenie pol");
		newItem.setMnemonic('S');
		newItem.setEnabled(true);
		newItem.addActionListener(this);
		propertiesMenu.add(newItem);		

		newItem = new JMenuItem("Zaczyna");
		newItem.setMnemonic('Z');
		newItem.setEnabled(true);
		newItem.addActionListener(this);
		propertiesMenu.add(newItem);		
	}
	
    private void createHelpMenu() {
		helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('H');

		JMenuItem newItem = new JMenuItem("Rules");
		newItem.setMnemonic('R');
		newItem.setEnabled(true);
		newItem.addActionListener(this);
		helpMenu.add(newItem);

		newItem = new JMenuItem("About ...");
		newItem.setMnemonic('A');
		newItem.setEnabled(true);
		newItem.addActionListener(this);
		helpMenu.add(newItem);
	}
    
    public void actionPerformed(ActionEvent event) {
		if (event.getActionCommand().equals("About ...")) about();
		else if (event.getActionCommand().equals("Rules")) rules();
		else if (event.getActionCommand().equals("Exit")) exitSystem();
		else if (event.getActionCommand().equals("New")) gameEngine.newGame();
		else JOptionPane.showMessageDialog(this,"Error in event handler","Error: ",JOptionPane.ERROR_MESSAGE);
	}
	 
	private void about() {   
		AboutDialog dialog = new AboutDialog(Interface.this);
		dialog.show();
	}

	private void rules() {   
		RulesDialog dialog = new RulesDialog(Interface.this);
		dialog.show();
	}
        
    private void exitSystem() {
        System.exit(0);
	}
	
}
