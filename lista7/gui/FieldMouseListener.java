package gui;

import java.awt.event.*;

public class FieldMouseListener implements MouseListener {
    private Field targetField;

    public FieldMouseListener(){
        super();
    }

    @Override public void mouseReleased(MouseEvent e) {}
    @Override public void mousePressed(MouseEvent e) {}
    @Override public void mouseExited(MouseEvent e) {}
    @Override public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseClicked(MouseEvent e) {
        try{
            targetField = (Field)e.getComponent();
            if (!targetField.isPawnShown()) targetField.showPawn();
        } catch(RuntimeException ex){}
    }
}