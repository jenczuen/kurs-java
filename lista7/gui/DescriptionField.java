package gui;

import java.awt.*;
import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import gameEngine.*;

public class DescriptionField extends JPanel{

  private JLabel label;

  public DescriptionField(int x, int y, int size){
    super();
    label = new JLabel();
    if(x==0 || x==size-1){
      if(y!=0 && y!=size-1)
        label.setText(""+y);  
    } else if (y==0 || y==size-1){
      label.setText(""+((char)((int)'A'+x-1)));
    }
    add(label);
  }

  public void show(){
    setVisible(true);
  }

  public void hide(){
    setVisible(false);
  }

}