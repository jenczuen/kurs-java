package narzedzia

public class TrojkatPascala{
	private final static int MAX = 67; //20 ???

	private static long[][] trojkat = new long[MAX][];

	static {
	        // utworzenie tablicy
	        for (int i=0; i<trojkat.length; i++)
	            trojkat[i] = new long[i+1];
	        //wypelnienie tablicy
	        trojkat[0][0] = 1;
		trojkat[1][0] = 1;
		trojkat[1][1] = 1;
	        for (int i=2; i<trojkat.length; i++){
	            trojkat[i][0] = 1;
	            for (int j=1; j<trojkat[i].length-1; j++) 	
	            	trojkat[i][j] = trojkat[i-1][j-1] + trojkat[i-1][j];
		    trojkat[i][i] = 1;
        	}

/*		for(int i=0;i<MAX;i++){
			for(int j=0;j<trojkat[i].length;j++){
				System.out.print(trojkat[i][j]+" ");
			}
			System.out.println();
		}*/
   	}

	public static long czytaj (int n, int k) throws IllegalArgumentException{
	        try{
			return trojkat[n][k];
      		} 
	        catch (ArrayIndexOutOfBoundsException ex){
	            throw new IllegalArgumentException();     
	        }
	}
}
