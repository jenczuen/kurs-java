import narzedzia.*;

public class WzorDwumianowy{

	public static void main(String args[]){
		try{
			int n = new Integer(args[1]);
			int p = args[0].indexOf("+");
			if (p==-1) p = args[0].indexOf("-");
			if (p==-1) throw new IllegalArgumentException();
			String first = args[0].substring(0,p);
			String second = args[0].substring(p+1);

			long a=0;
			String x;
			String y;
			String skladnik;
			String znak="";
			char znak_ogolny = args[0].charAt(p);
			for(int k=0;k<=n;k++){
				a = TrojkatPascala.czytaj(n,k);
				
				if(a!=1) x=String.valueOf(a)+"*";
				else x="";
				if(n-k==1) x+=first+"*";
				else if(k==0) x+=first+"^"+Integer.toString(n);
				else if(n-k==0) x+="";
				else x+=first+"^"+Integer.toString(n-k)+"*";

				if(k==0) y="";
				else if(k==1) y=second;
				else y=second+"^"+Integer.toString(k);

				if(k==n) znak="";
				else if(znak_ogolny=='+') znak=" + ";
				else if(k%2==1) znak=" + ";
				else znak = " - ";

				skladnik = x+y+znak;
				System.out.print(skladnik);
			}
			System.out.println();
		}
		catch (ArrayIndexOutOfBoundsException ex){
			System.out.println("Zla ilość argumentów!\n");
		}
		catch (IllegalArgumentException ex){
			System.out.println("Błędne argumenty!\n");
		}

	}
}
