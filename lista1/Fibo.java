public class Fibo
{
    private static String infoMessage = "Poprawne wywołanie:\n\n"+
    									"$ java Fibo n a b\n\n"+
    									"gdzie n to nieduża liczba naturalna > 2,\n"+
    									"natomiast a i b to dwa różne słowa jednoliterowe.";

    private static String fib[];
    private static int n;
    private static String a,b;

    private static void generate(){
    	fib = new String[n+1];
    	fib[0] = "";
    	fib[1] = a;
    	fib[2] = b;    	
    	for (int i=3; i<fib.length; i++)
    		fib[i] = fib[i-2] + fib[i-1];
    }    

    public static void main (String[] args)
    {
    	//blok wczytywania danych
		try {
            a = args[1];
            b = args[2];            
            n = new Integer(args[0]);

            if (n <= 2) throw new NumberFormatException(); 
			if (a.compareTo(b)==0) throw new IllegalArgumentException();
			if (a.length()!=1||b.length()!=1) throw new IllegalArgumentException();

			//generowanie ciagu
			generate();

			//drukowanie ostatniego elementu
			System.out.println(fib[n]);
		}
		catch (ArrayIndexOutOfBoundsException ex){
			System.out.println("Błąd wywołania - zła ilość argumentów!\n");
			System.out.println(infoMessage);
		}
		catch (NumberFormatException ex) {
			System.out.println("Błąd wywołania - pierwszym argumentem powinna być liczba naturalna > 2!\n");
			System.out.println(infoMessage);	
		}
		catch (IllegalArgumentException ex) {
			System.out.println("Błąd wywołania - słowa powinny być jednoliterowe i różne!\n");
			System.out.println(infoMessage);
		}
    }
}
