/*
  Copyright (c) October 2010 by Pawe³ Rzechonek
  Program wypisuje na standardowym wyj¶ciu napis zawieraj±cy wszystkie polskie
  znaki diakrytyczne.
  Istotne elementy w programie:
    * aby program poprawnie skompilowaæ u¿yj opcji -encoding iso-8859-2 (takie jest kodowanie pliku ¿ród³owego).
*/

public class PolskieZnakiISO88592
{
    public static void main (String[] args)
    {
        System.out.println("Pozb±d¼ siê z³udzeñ i nie z³o¶æ na pró¿no!");
        System.out.println("POZB¡D¬ SIÊ Z£UDZEÑ I NIE Z£O¦Æ NA PRÓ¯NO!");
        System.out.println("Pozbądź się złudzeń i nie złość na próżno!");
    }
}

