import java.io.*;
import narzedzia.obliczenia.*;
import narzedzia.kontenery.*;
import narzedzia.wyjatki.*;

public class Kalkulator {
	public static void main(String args[]){
		BufferedReader we = new BufferedReader(new InputStreamReader(System.in));		
		boolean koniec = false;
		String komenda;
		String onp;
		Wyrazenie w;
		Zbior zmienne = new Zbior();
		String[] tab;

		do{
			try{
				System.out.print("komenda: ");
				komenda = we.readLine().trim();
				if(komenda.equals("exit")){
					koniec = true;
				} else {
					tab = komenda.trim().split("\\s");
					if(tab[0].equals("calc")){
						onp = komenda.substring(4);
						w = new Wyrazenie(onp,zmienne);
						System.out.println(w.oblicz());
					} else
					if(tab[0].equals("clear")){
						int i;
						for(i=1; i<tab.length; i++){
							System.out.println("usuwam "+tab[i]);
							zmienne.usun(tab[i]);
						}
						if(i==1)zmienne.czysc();
					}
				}
			} 
			catch(ONP_DzieleniePrzez0 ex) {
				System.out.println("Dzielenie przez zero!");
			}		
			catch(ONP_BledneWyrazenie ex) {
				System.out.println("Bledne wyrazenie!");
			}
			catch(ONP_NieznanySymbol ex) {
				System.out.println("Nieznany symbol!");
			}
			catch(ONP_PustyStos ex) {
				System.out.println("Jeden z operatorow ma za malo argumentow!");
			}
			catch(WyjatekONP ex) {
				System.out.println("Nieznany blad!");
			}
		    catch (IOException ex) {
		    	koniec = true;
		    	System.err.println("Error: " + ex);
		    }
		} while(!koniec);
	}
}