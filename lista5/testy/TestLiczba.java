package testy;

import narzedzia.obliczenia.*;
import narzedzia.wyjatki.*;

public class TestLiczba extends Test{
	public void testuj(){
		System.out.println("--------------- Test Liczba");		
		Liczba liczba = new Liczba(4.4);

		System.out.println("Wartosc liczby : ");

		try{
			System.out.println(liczba.obliczWartosc());
		}
		catch (WyjatekONP ex) {}

		System.out.println("================= koniec Test Liczba");
	}
}