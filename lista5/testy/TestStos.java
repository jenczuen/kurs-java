package testy;

import narzedzia.kontenery.*;
import narzedzia.wyjatki.*;

public class TestStos extends Test{
	public void testuj(){
		System.out.println("--------------- Test Stos");		

		try{
			Stos<Double> stos = new Stos<Double>();

			System.out.println("Ilosc elementow na stosie: ");
			System.out.println(stos.count());
			System.out.println("Proba zdjecia elementu z pustego stosu: ");
			stos.pop();
			System.out.println("Ilosc elementu po wlozeniu na stos: ");
			stos.push(5.5);
			System.out.println(stos.count());		
			System.out.println("Ilosc elementu po wlozeniu na stos: ");
			stos.push(2.2);
			System.out.println(stos.count());		
			System.out.println("Ilosc elementu po wlozeniu na stos: ");
			stos.push(4.2);
			System.out.println(stos.count());		
			System.out.println("Zdjecie elementu ze stosu: ");
			System.out.println(stos.pop());
			System.out.println("Zdjecie elementu ze stosu: ");
			System.out.println(stos.pop());
			System.out.println("Zdjecie elementu ze stosu: ");
			System.out.println(stos.pop());
			System.out.println("Zdjecie elementu ze stosu: ");
			System.out.println(stos.pop());
			System.out.println("================= koniec Test Zbior");				
		}
		catch (WyjatekONP ex) {}
	}
}