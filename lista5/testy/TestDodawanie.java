package testy;

import narzedzia.obliczenia.*;
import narzedzia.wyjatki.*;

public class TestDodawanie extends Test{
	public void testuj(){
		System.out.println("--------------- Test Dodawanie");		
		Dodawanie dodawanie = new Dodawanie();

		System.out.println("Arnosc funkcji dodawanie:");
		System.out.println(dodawanie.arnosc());
		System.out.println("Ilosc brakujacych elemntow:");
		System.out.println(dodawanie.brakujaceArgumenty());
		System.out.println("Dodanie arg1 = 3.3 ...");
		try{
			dodawanie.dodajArgument(3.3);	
		} catch (WyjatekONP ex) {} 
		
		System.out.println("Ilosc brakujacych elemntow:");
		System.out.println(dodawanie.brakujaceArgumenty());
		System.out.println("Dodanie arg2 = 1.1 ...");
		try{
			dodawanie.dodajArgument(1.1);	
		} catch (WyjatekONP ex) {} 
		System.out.println("Ilosc brakujacych elemntow:");
		System.out.println(dodawanie.brakujaceArgumenty());
		System.out.println("Obliczenie wartosci funkcji:");
		try{
			System.out.println(dodawanie.obliczWartosc());
		} catch (WyjatekONP ex) {} 

		System.out.println("================= koniec Test Dodawanie");
	}
}