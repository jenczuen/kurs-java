package testy;

import narzedzia.kontenery.*;
import narzedzia.obliczenia.*;
import narzedzia.wyjatki.*;

public class TestKolejka extends Test{
	public void testuj(){
		System.out.println("--------------- Test kolejka");		
		Kolejka<Liczba> kolejka = new Kolejka<Liczba>();

		System.out.println("Ilosc elementow na kolejce: ");
		System.out.println(kolejka.count());
		System.out.println("Proba zdjecia elementu z pustego kolejkau: ");
		kolejka.get();
		System.out.println("Ilosc elementu po wlozeniu na kolejka: ");
		kolejka.push(new Liczba(new Double(5.5)));
		System.out.println(kolejka.count());		
		System.out.println("Ilosc elementu po wlozeniu na kolejka: ");
		kolejka.push(new Liczba(new Double(2.2)));
		System.out.println(kolejka.count());		
		System.out.println("Ilosc elementu po wlozeniu na kolejka: ");
		kolejka.push(new Liczba(new Double(4.2)));
		System.out.println(kolejka.count());		
		System.out.println("Zdjecie elementu ze kolejkau: ");
		try{
			System.out.println(kolejka.get().obliczWartosc());
		}
		catch (WyjatekONP ex) {}
		System.out.println("Zdjecie elementu ze kolejkau: ");
		try{
			System.out.println(kolejka.get().obliczWartosc());
		}
		catch (WyjatekONP ex) {}
		System.out.println("Zdjecie elementu ze kolejkau: ");
		try{
			System.out.println(kolejka.get().obliczWartosc());
		}
		catch (WyjatekONP ex) {}
		System.out.println("Zdjecie elementu ze kolejkau: ");
		System.out.println(kolejka.get());

		System.out.println("================= koniec Test Zbior");				
	}
}