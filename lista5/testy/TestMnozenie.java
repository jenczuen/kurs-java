package testy;

import narzedzia.obliczenia.*;
import narzedzia.wyjatki.*;

public class TestMnozenie extends Test{
	public void testuj(){
		System.out.println("--------------- Test Mnozenie");		
		Mnozenie mnozenie = new Mnozenie();
		try{
			System.out.println("Arnosc funkcji mnozenie:");
			System.out.println(mnozenie.arnosc());
			System.out.println("Ilosc brakujacych elemntow:");
			System.out.println(mnozenie.brakujaceArgumenty());
			System.out.println("Dodanie arg1 = 3.3 ...");
			mnozenie.dodajArgument(3.3);
			System.out.println("Ilosc brakujacych elemntow:");
			System.out.println(mnozenie.brakujaceArgumenty());
			System.out.println("Dodanie arg2 = 1.1 ...");
			mnozenie.dodajArgument(1.1);
			System.out.println("Ilosc brakujacych elemntow:");
			System.out.println(mnozenie.brakujaceArgumenty());
			System.out.println("Obliczenie wartosci funkcji:");
			System.out.println(mnozenie.obliczWartosc());			
		}
		catch (WyjatekONP ex) {}

		System.out.println("================= koniec Test Mnozenie");
	}
}