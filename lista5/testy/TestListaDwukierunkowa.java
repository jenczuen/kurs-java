package testy;

import narzedzia.kontenery.*;
import narzedzia.obliczenia.*;

public class TestListaDwukierunkowa extends Test{
	public void testuj(){
		System.out.println("--------------- Test Lista Dwukierukowa");		
		ListaDwukierunkowa<Double> lista = new ListaDwukierunkowa<Double>();

		System.out.println("Ilosc elementow na liscie: ");
		System.out.println(lista.count());
		System.out.println("Proba zdjecia elementu z przodu z pustej listy: ");
		lista.zdejmijZPoczatku();
		System.out.println("Proba zdjecia elementu z konca z pustej listy: ");
		lista.zdejmijZKonca();

		System.out.println("Ilosc elementu po wlozeniu na poczatek listy: ");
		lista.dodajNaPoczatek(1.1);
		// [1.1]
		System.out.println(lista.count());		
		System.out.println("Ilosc elementu po wlozeniu na koniec listy: ");
		lista.dodajNaKoniec(1.9);
		// [1.1, 1.9]
		System.out.println(lista.count());		

		System.out.println("Ilosc elementu po wlozeniu na poczatek listy: ");
		lista.dodajNaPoczatek(2.1);
		// [2.1, 1.1, 1.9]
		System.out.println(lista.count());		
		System.out.println("Ilosc elementu po wlozeniu na koniec listy: ");
		lista.dodajNaKoniec(2.9);
		// [2.1, 1.1, 1.9, 2.9]
		System.out.println(lista.count());		

		System.out.println("Ilosc elementu po wlozeniu na poczatek listy: ");
		lista.dodajNaPoczatek(3.1);
		// [3.1, 2.1, 1.1, 1.9, 2.9]
		System.out.println(lista.count());		
		System.out.println("Ilosc elementu po wlozeniu na koniec listy: ");
		lista.dodajNaKoniec(3.9);
		// [3.1, 2.1, 1.1, 1.9, 2.9, 3.9]
		System.out.println(lista.count());		

		System.out.println("Zdjecie elementu ze przodu listy: ");
		System.out.println(lista.zdejmijZPoczatku());
		System.out.println("Zdjecie elementu ze przodu listy: ");
		System.out.println(lista.zdejmijZPoczatku());
		System.out.println("Zdjecie elementu ze przodu listy: ");
		System.out.println(lista.zdejmijZPoczatku());
		System.out.println("Zdjecie elementu ze przodu listy: ");
		System.out.println(lista.zdejmijZPoczatku());

		System.out.println("Zdjecie elementu ze konca listy: ");
		System.out.println(lista.zdejmijZKonca());
		System.out.println("Zdjecie elementu ze konca listy: ");
		System.out.println(lista.zdejmijZKonca());
		System.out.println("Zdjecie elementu ze konca listy: ");
		System.out.println(lista.zdejmijZKonca());

		System.out.println("================= koniec Test Lista Dwukierukowa");				
	}
}