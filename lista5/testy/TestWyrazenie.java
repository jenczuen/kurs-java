package testy;

import narzedzia.obliczenia.*;
import narzedzia.kontenery.*;
import narzedzia.wyjatki.*;

public class TestWyrazenie extends Test{
	public void testuj(){
		System.out.println("--------------- Test Wyrazenie");		
		Zbior zmienne = new Zbior();
		
		try{
			//(((2.1+3.2)*(1.3+4.4))+(3.5*9.6))/0.2
			// 2.1 3.2 + 1.3 4.4 + * 3.5 9.6 * + 0.2 /
			Wyrazenie w = new Wyrazenie(new String("2.1 3.2 + 1.3 4.4 + * 3.5 9.6 * + -0.2 / Abs Sgn"),zmienne);
			System.out.println(w.oblicz());
		} 
		catch(ONP_DzieleniePrzez0 ex) {
			System.out.println("Dzielenie przez zero!");
		}		
		catch(ONP_BledneWyrazenie ex) {
			System.out.println("Bledne wyrazenie!");
		}
		catch(ONP_NieznanySymbol ex) {
			System.out.println("Nieznany symbol!");
		}
		catch(ONP_PustyStos ex) {
			System.out.println("Jeden z operatorow ma za malo argumentow!");
		}
		catch(WyjatekONP ex) {
			System.out.println("Nieznany blad!");
		}


		System.out.println("================= koniec Test Wyrazenie");
	}
}