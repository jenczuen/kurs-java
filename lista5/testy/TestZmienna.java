package testy;

import narzedzia.obliczenia.*;
import narzedzia.kontenery.*;

public class TestZmienna extends Test{
	public void testuj(){
		System.out.println("--------------- Test Zmienna");		
		Zbior zbior = new Zbior();
		Zmienna.zbior = zbior;
		Zmienna x = new Zmienna("x");
		Zmienna y = new Zmienna("y");		

		System.out.println("Wartosc zmiennej x:");				
		System.out.println(x.obliczWartosc());
		System.out.println("Wartosc zmiennej y:");				
		System.out.println(y.obliczWartosc());

		System.out.println("Ustalanie...");		
		x.ustal(5.5);
		y.ustal(2.2);

		System.out.println("Wartosc zmiennej x:");				
		System.out.println(x.obliczWartosc());
		System.out.println("Wartosc zmiennej y:");				
		System.out.println(y.obliczWartosc());

		System.out.println("================= koniec Test Zmienna");		
	}
}