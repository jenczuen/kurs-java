

class Disc < ActiveRecord::Base
	has_many :songs
	belongs_to :author
end

class Song < ActiveRecord::Base
	belongs_to :disc
	belongs_to :author
end

class Author < ActiveRecord::Base
	has_many :discs
	has_many :songs, :through => :discs
end