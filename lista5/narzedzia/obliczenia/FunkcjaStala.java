package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public abstract class FunkcjaStala extends Function{

	public FunkcjaStala(){}

	public int arnosc(){
		return 0;
	}
	
	public int brakujaceArgumenty(){
		return 0;
	}
	
	public void dodajArgument(double a) throws WyjatekONP {}

	public abstract double obliczWartosc() throws WyjatekONP;
}