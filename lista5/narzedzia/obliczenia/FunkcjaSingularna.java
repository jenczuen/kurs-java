package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public abstract class FunkcjaSingularna extends Function{
	protected Double arg;

	public FunkcjaSingularna(){
		arg = null;
	}

	public int arnosc(){
		return 1;
	}
	
	public int brakujaceArgumenty(){
		if(arg == null) return 1;
		return 0;
	}
	
	public void dodajArgument(double a) throws WyjatekONP {
		if(arg == null){
			arg = new Double(a);
		} else {
			throw new ONP_BledneWyrazenie();
		}
	}

	public abstract double obliczWartosc() throws WyjatekONP;
}