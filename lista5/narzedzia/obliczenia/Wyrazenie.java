package narzedzia.obliczenia;

import narzedzia.kontenery.*;
import narzedzia.wyjatki.*;

public class Wyrazenie{
	private Kolejka<Symbol> kolejka;
	private Stos<Double> stos;
	private Zbior zmienne;
	private String wyrazenie;

	public Wyrazenie(String onp, Zbior zm) throws WyjatekONP{
		kolejka = new Kolejka<Symbol>();
		stos = new Stos<Double>();
		zmienne = new Zbior();
		wyrazenie = onp;
		zmienne = zm;
		Zmienna.zbior = zmienne;
	}

	public double oblicz() throws WyjatekONP{
		try{
			parsuj();
		
			Symbol symbol = kolejka.get();
			double wynik = 0;
			double liczba;
			Kolejka<Symbol> zmienneNZ = new Kolejka<Symbol>();
			while(symbol != null){
				if(symbol instanceof Function){
					while(((Function)symbol).brakujaceArgumenty() > 0){
						((Function)symbol).dodajArgument(stos.pop());
					}
					stos.push(((Function)symbol).obliczWartosc());
				} else 
				if(symbol instanceof Operand){
					if(symbol instanceof ZmiennaNiezainicjowana){
						zmienneNZ.push(symbol);
					} else stos.push(((Operand)symbol).obliczWartosc());	
				}
				symbol = kolejka.get();
			}
			if(stos.count() > 1) throw new ONP_BledneWyrazenie();
			while(zmienneNZ.count() > 0){
				symbol = zmienneNZ.get();
				((Zmienna)symbol).ustal(stos.end());
			}
			return stos.pop();
		}
		catch(ONP_DzieleniePrzez0 ex) {
			throw new ONP_DzieleniePrzez0();
		}
		catch(ONP_BledneWyrazenie ex) {
			throw new ONP_BledneWyrazenie();
		}
		catch(ONP_NieznanySymbol ex) {
			throw new ONP_NieznanySymbol();
		}
		catch(ONP_PustyStos ex) {
			throw new ONP_PustyStos();
		}
		catch(WyjatekONP ex) {
			throw new WyjatekONP();
		}
	}

	private void parsuj() throws WyjatekONP{
		String symbole[] = wyrazenie.trim().split("\\s+");
		String symbol_string;
		for(int i=0;i<symbole.length;i++){
			symbol_string = symbole[i];
			if(symbol_string.matches("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?")){
				kolejka.push(new Liczba(new Double(symbol_string)));
			} else 
			if(symbol_string.equals("+")){
				kolejka.push(new Dodawanie());
			} else 
			if(symbol_string.equals("-")){
				kolejka.push(new Odejmowanie());
			} else 
			if(symbol_string.equals("*")){
				kolejka.push(new Mnozenie());
			} else 
			if(symbol_string.equals("/")){
				kolejka.push(new Dzielenie());
			} else 
			if(symbol_string.equals("Min")){
				kolejka.push(new Min());
			} else 
			if(symbol_string.equals("Max")){
				kolejka.push(new Max());
			} else
			if(symbol_string.equals("Abs")){
				kolejka.push(new Abs());
			} else 
			if(symbol_string.equals("Sgn")){
				kolejka.push(new Sgn());
			} else
			if(symbol_string.equals("E")){
				kolejka.push(new E());
			} else
			if(symbol_string.equals("Pi")){
				kolejka.push(new Pi());
			} else
			if(symbol_string.matches("\\p{Alpha}\\p{Alnum}*")){
				if(symbole.length > 1){
					if(symbole[i+1].equals("=")){
						kolejka.push(new ZmiennaNiezainicjowana(symbol_string));
						i++;
					} else {
						kolejka.push(new Zmienna(symbol_string));
					}					
				} else kolejka.push(new Zmienna(symbol_string));
			} else throw new ONP_NieznanySymbol();
		}
	}
}