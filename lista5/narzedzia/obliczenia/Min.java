package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public class Min extends FunkcjaBinarna{
	public double obliczWartosc() throws WyjatekONP {
		if(arg1 != null && arg2 != null){
			if(arg2 < arg1) return arg2;
			else return arg1;
		} else {
			throw new ONP_BledneWyrazenie();
		}
	}
}