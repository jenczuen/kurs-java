package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public interface IObliczalny{
	double obliczWartosc() throws WyjatekONP;
}