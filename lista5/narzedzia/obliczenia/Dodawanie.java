package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public class Dodawanie extends FunkcjaBinarna{
	public double obliczWartosc() throws WyjatekONP {
		if(arg1 != null && arg2 != null){
			return arg1 + arg2;
		} else {
			throw new ONP_BledneWyrazenie();
		}
	}
}