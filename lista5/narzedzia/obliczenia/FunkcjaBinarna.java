package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public abstract class FunkcjaBinarna extends Function{
	protected Double arg1;
	protected Double arg2;

	public FunkcjaBinarna(){
		arg1 = null;
		arg2 = null;
	}

	public int arnosc(){
		return 2;
	}
	
	public int brakujaceArgumenty(){
		if(arg1 == null && arg2 == null) return 2;
		if(arg1 == null || arg2 == null) return 1;
		return 0;
	}
	
	public void dodajArgument(double arg) throws WyjatekONP {
		if(arg1 == null){
			arg1 = new Double(arg);
		} else {
			if(arg2 == null){
				arg2 = new Double(arg);
			} else {
				throw new ONP_BledneWyrazenie();
			}
		}
	}

	public abstract double obliczWartosc() throws WyjatekONP;
}