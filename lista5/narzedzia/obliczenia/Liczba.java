package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public class Liczba extends Operand{
	private double value;

	public Liczba(double v){
		value = v;	
	}

	public double obliczWartosc() throws WyjatekONP {
		return value;
	}
}