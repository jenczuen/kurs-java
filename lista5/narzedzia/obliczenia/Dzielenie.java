package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public class Dzielenie extends FunkcjaBinarna{
	public double obliczWartosc() throws WyjatekONP {
		if(arg1 != null && arg2 != null){
			if(arg1 == 0.0) throw new ONP_DzieleniePrzez0();
			return arg2 / arg1;
		} else {
			throw new ONP_BledneWyrazenie();
		}
	}
}