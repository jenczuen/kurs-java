package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public class Odejmowanie extends FunkcjaBinarna{
	public double obliczWartosc() throws WyjatekONP {
		if(arg1 != null && arg2 != null){
			return arg2 - arg1;
		} else {
			throw new ONP_BledneWyrazenie();
		}
	}
}