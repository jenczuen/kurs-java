package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public abstract class Function extends Symbol implements IFunkcyjny{
	public abstract double obliczWartosc() throws WyjatekONP;
	public abstract int arnosc();
	public abstract int brakujaceArgumenty();
	public abstract void dodajArgument(double arg) throws WyjatekONP;
}