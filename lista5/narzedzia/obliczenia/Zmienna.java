package narzedzia.obliczenia;

import narzedzia.kontenery.*;

public class Zmienna extends Operand{
	public static Zbior zbior;
	protected String klucz;

	public Zmienna(String kl){
		klucz = kl;
		zbior.wstaw(klucz,0);
	}

	public void ustal(double wartosc){
		zbior.przypisz(klucz,wartosc);
	}

	public double obliczWartosc(){
		return zbior.szukaj(klucz);
	}

	public String to_string(){
		return klucz;
	}
}