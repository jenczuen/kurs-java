package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public class Abs extends FunkcjaSingularna{
	public double obliczWartosc() throws WyjatekONP {
		if(arg != null){
			if(arg >= 0) return arg;
			return -arg;
		} else {
			throw new ONP_BledneWyrazenie();
		}
	}
}