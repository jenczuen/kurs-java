package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public class Pi extends FunkcjaStala{
	public double obliczWartosc() throws WyjatekONP {
		return 3.14159;
	}
}