package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public interface IFunkcyjny extends IObliczalny{
	int arnosc();
	int brakujaceArgumenty();
	void dodajArgument(double arg) throws WyjatekONP;
}