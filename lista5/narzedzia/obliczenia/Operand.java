package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public abstract class Operand extends Symbol implements IObliczalny{
	public abstract double obliczWartosc() throws WyjatekONP;
}