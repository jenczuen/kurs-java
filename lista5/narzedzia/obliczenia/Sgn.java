package narzedzia.obliczenia;

import narzedzia.wyjatki.*;

public class Sgn extends FunkcjaSingularna{
	public double obliczWartosc() throws WyjatekONP {
		if(arg != null){
			if(arg > 0) return 1;
			if(arg < 0) return -1;
			return 0;
		} else {
			throw new ONP_BledneWyrazenie();
		}
	}
}