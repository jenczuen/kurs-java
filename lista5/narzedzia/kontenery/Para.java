package narzedzia.kontenery;

public class Para{
	public final String klucz;
	private double wartosc;

	public Para(String k){
		klucz = k;
	}

	public Para(String k, double w){
		klucz = k;
		wartosc = w;
	}

	public void setWartosc(double w){
		wartosc = w;
	}

	public double getWartosc(){
		return wartosc;
	}

}	