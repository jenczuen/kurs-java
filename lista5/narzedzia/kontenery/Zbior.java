package narzedzia.kontenery;

import java.util.*;

public class Zbior{
	private List<Para> list;

	public Zbior(){
		list = new LinkedList<Para>();
	}

	public void wstaw(String kl, double wart) throws IllegalArgumentException{
		list.add(new Para(kl,wart));			
	}

	public void przypisz(String kl, double wart) throws IllegalArgumentException{
		Iterator<Para> itr = list.iterator();
		Para p;
		try{
			while(itr.hasNext()){
				p = itr.next();
				if( p.klucz.equals(kl) ){
					p.setWartosc(wart);
				}
			}
		}
		catch (IllegalArgumentException ex){
			throw new IllegalArgumentException();	
		}
	}
	
	public double szukaj(String kl) throws IllegalArgumentException{
		Iterator<Para> itr = list.iterator();
		Para p;
		try{
			while(itr.hasNext()){
				p = itr.next();
				if( p.klucz.equals(kl) ){
					return p.getWartosc();
				}
			}
			throw new IllegalArgumentException();
		}
		catch (IllegalArgumentException ex){
			throw new IllegalArgumentException();	
		}
	}

	public void usun(String kl) throws IllegalArgumentException{
		Iterator<Para> itr = list.iterator();
		Para p;
		try{
			while(itr.hasNext()){
				p = itr.next();
				if( p.klucz.equals(kl) ){
					itr.remove();
					return;
				}
			}
			throw new IllegalArgumentException();
		}
		catch (IllegalArgumentException ex){
			throw new IllegalArgumentException();	
		}
	}

	public int ile(){
		return list.size();
	}

	public void czysc(){
		list.clear();
	}
}