package narzedzia.kontenery;

import narzedzia.wyjatki.*;
 
public class Stos <ContentType> {
	ListaDwukierunkowa<ContentType> lista = new ListaDwukierunkowa<ContentType>();
	 	  
	public void push(ContentType x){
		lista.dodajNaKoniec(x);
	}
		 
	public ContentType pop() throws WyjatekONP{
		ContentType koniec = lista.zdejmijZKonca();
		if(koniec == null){
			throw new ONP_PustyStos();
		}
		return koniec;
	}
	 
	public ContentType end() throws WyjatekONP{
		ContentType koniec = lista.zwrocKoniec();
		if(koniec == null){
			throw new ONP_PustyStos();
		}
		return koniec;
	}

	public int count(){
		return lista.count();
	}

}