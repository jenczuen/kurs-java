package narzedzia.kontenery;
 
public class ListaDwukierunkowa <ContentType>{
 	Elem begin;
 	Elem end;
	 
	public ListaDwukierunkowa(){
		begin = null;
		end = null;
	}

	public ContentType zwrocPoczatek(){
		return begin.getContent();
	}

	public ContentType zwrocKoniec(){
		return end.getContent();
	}
	  
	public void dodajNaKoniec(ContentType content){
		Elem e = new Elem(content);
		if (begin == null && end == null){
			e.setNext(null);
			e.setPrev(null);
			begin = e;
			end = e;
		} else {
			end.setNext(e);
			e.setPrev(end);
			end = e;
		}
	}

	public void dodajNaPoczatek(ContentType content){
		Elem e = new Elem(content);
		if (begin == null && end == null){
			e.setNext(null);
			e.setPrev(null);
			begin = e;
			end = e;
		} else {
			begin.setPrev(e);
			e.setNext(begin);
			begin = e;
		}
	}

	public ContentType zdejmijZKonca(){
		if (end != null){
			ContentType content;
			content = end.getContent();
			if (end.getPrev() != null){
				end = end.getPrev();
				end.setNext(null);			
			} else {
				begin = null;
				end = null;
			}
			return content;
		} else {
			return null;
		}
	}
	 
	public ContentType zdejmijZPoczatku(){
		if (begin != null){
			ContentType content;
			content = begin.getContent();
			if (begin.getNext() != null){
				begin = begin.getNext();
				begin.setPrev(null);			
			} else {
				begin = null;
				end = null;
			}
			return content;
		} else {
			return null;
		}
	}
	 
	public int count(){
		if(begin!=null){
			Elem tmp = begin;
			int counter = 0;
			while(tmp != null){
				counter++;
				tmp = tmp.getNext();
			}
			return counter;
		}
		else return 0;
	}

	private class Elem {
		ContentType content;
		Elem next;
		Elem prev;		
		 
		public Elem(ContentType c){
			content = c;
			next = null;
			prev = null;		
		}

		public void setNext(Elem n){
			next = n;
		}

		public void setPrev(Elem s){
			prev = s;
		}
		 
		public Elem getNext(){
			return next;
		}

		public Elem getPrev(){
			return prev;
		}
		 
		public ContentType getContent(){
			return content;
		}
	}
}