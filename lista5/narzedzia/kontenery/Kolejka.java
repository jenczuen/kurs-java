package narzedzia.kontenery;

import narzedzia.obliczenia.*;
 
public class Kolejka <ContentType> {
	ListaDwukierunkowa<ContentType> lista = new ListaDwukierunkowa<ContentType>();	
	  
	public void push(ContentType content){
		lista.dodajNaKoniec(content);
	}
	 
	public ContentType get(){
		return lista.zdejmijZPoczatku();
	}
	 
	public int count(){
		return lista.count();
	}
}