package narzedzia;

public class RzymArab {

	public static int rzym2arab(String liczba) throws WyjatekKonwersji{
		Stos<Integer> stos = new Stos<Integer>();
		String tab[] = liczba.split("");
		int wynik = 0;
		int cyfra = 0;
		int tmp = 0;
		for(int i=1; i < tab.length; i++){
			cyfra = symbolToInt(tab[i].charAt(0));
			if(stos.top() == null) stos.push(cyfra);
			else {	
				if(stos.top() < cyfra){
					tmp = stos.pop();
					cyfra -= tmp;
					if(tmp == stos.top()){
						stos.pop();
						cyfra -= tmp;
						if(tmp == stos.top()) throw new WyjatekKonwersji();
					}
					stos.push(cyfra);
				} else stos.push(cyfra);
			}
		}

		while(stos.top() != null) wynik += stos.pop();
		
		return wynik;
	}

	public static String arab2rzym(int liczba) throws WyjatekKonwersji{
		int rzad = 1000;
		int cyfra = 0;
		char[] wynik = new char[20];
		char[] wynik_posredni;
		int offset = 0;
		try{
			while(liczba > 0){
				cyfra = liczba / rzad;
				if(cyfra > 0){
					wynik_posredni = wynikPosredni(cyfra, rzad);
					for(int i = 0; i<wynik_posredni.length; i++){
						wynik[i+offset++] = wynik_posredni[i];
					}	
				}
				liczba -= cyfra * rzad;
				rzad = rzad / 10;	
			}

			for(int i = 0; i<offset; i++){
				System.out.print(wynik[i]);
			}	

			return "test";
		} catch(WyjatekKonwersji ex){
			throw new WyjatekKonwersji();
		}
	}

	private static char[] wynikPosredni(int cyfra, int rzad) throws WyjatekKonwersji{
		try{
			int a1,a2,b1,b2;
			if(cyfra == 1 || cyfra == 5){
				char[] wynik = new char[1];
				wynik[0] = intToSymbol(intToSymbol(cyfra));
				return wynik;
			} else {
				if(cyfra < 5) {a1 = 1; a2 = 5;}
				else {a1 = 5; a2 = 10;}
				b1 = cyfra - a1;
				b2 = a2 - cyfra;
				char[] wynik = new char[b1+1];
				if(b1 > b2){
					wynik[0] = intToSymbol(a1*rzad);
					for(int i=1; i<=b1; i++) wynik[i] = intToSymbol(intToSymbol(rzad));
				} else {
					wynik[b1] = intToSymbol(a2*rzad);
					for(int i=0; i<=b1-1; i++) wynik[i] = intToSymbol(intToSymbol(rzad));
				}
				return wynik;
			}
		} catch(WyjatekKonwersji ex){
			throw new WyjatekKonwersji();
		}
	}

	private static int symbolToInt(char symbol) throws WyjatekKonwersji{
		switch(symbol){
			case 'I': return 1;
			case 'V': return 5;
			case 'X': return 10;
			case 'L': return 50;
			case 'C': return 100;
			case 'D': return 500;
			case 'M': return 1000;
			default: throw new WyjatekKonwersji();
		}
	}

	private static char intToSymbol(int number) throws WyjatekKonwersji{
		switch(number){
			case 1: return 'I';
			case 5: return 'V';
			case 10: return 'X';
			case 50: return 'L';
			case 100: return 'C';
			case 500: return 'D';
			case 1000: return 'M';
			default: throw new WyjatekKonwersji();
		}
	}
}