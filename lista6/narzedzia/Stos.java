package narzedzia;

import java.util.*;
 
public class Stos<ContentType> {
 	Elem top;
	 
	public Stos(){
		top = null;
	}
	  
	public ContentType top(){
		if(top!=null){
			return top.getValue();
		} else {
			return null;
		}
	}

	public void push(ContentType x){
		top = new Elem(x,top);
	}
	 
	public ContentType pop(){
		if(top != null){
			ContentType value;
			value = top.getValue();
			top = top.getNext();
			return value;
		} else {
			System.out.println("Stos jest pusty");
			return null;
		}
	}
	 
	public int count(){
		if(top!=null){
			Elem tmp = top;
			int counter = 0;
			while(tmp != null){
				counter++;
				tmp = tmp.getNext();
			}
			return counter;
		}
		else return 0;
	}

	/* single element of the stack */
	private class Elem {
		ContentType value;
		Elem next;
		 
		public Elem(ContentType x, Elem n){
			next = n;
			value = x;
		}
		 
		public Elem getNext(){
			return next;
		}
		 
		public ContentType getValue(){
			return value;
		}
	}
}