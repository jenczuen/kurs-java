package narzedzia;

import java.util.regex.*;

public class RzymArab{
    private RzymArab() {}

    public static String arab2rzym(int liczba) throws WyjatekKonwersji{
        if (liczba < 0 || liczba > 3999) 
            throw new WyjatekKonwersji();

        String wynik = new String("");

        while (liczba > 0){
            for (int i=0; i<wartosciarabskie.length; i++)
                if ( wartosciarabskie[i] <= liczba ){
                            liczba -= wartosciarabskie[i];
                            wynik += wartoscirzymskie[i];
                }
        }

        return wynik;
    }

    public static int rzym2arab(String liczba) throws WyjatekKonwersji{
        Pattern pattern = Pattern.compile("[IVXLCDM]+");
        Matcher matcher = pattern.matcher(liczba);
        
        if ( matcher.matches() == false)
            throw new WyjatekKonwersji();
        
        int wynik = 0;
        int iter = 0;

        for (int i=0; i<wartoscirzymskie.length; i++)
            while (liczba.startsWith(wartoscirzymskie[i], iter)){
                wynik += wartosciarabskie[i];
                iter += wartoscirzymskie[i].length();
            }

        return wynik;
    }

    private static final int wartosciarabskie[] =
    { 2000,1000,900,800,700,600,500,400,300,200,100,90,80,70,60,50,40,30,20,10,9,8,7,6,5,4,3,2,1};
    
    private static final String wartoscirzymskie[] =
    {"MM", "M", "CM", "DCCC", "DCC", "DC", "D", "CD", "CCC", "CC", "C", "XC", "LXXX", "LXX", "LX", "L", "XL", "XXX", "XX", "X", "IX", "VIII", "VII", "VI", "V", "IV", "III", "II", "I"};
}