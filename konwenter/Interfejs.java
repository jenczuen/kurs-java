import narzedzia.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.*;
import java.io.*;

public class Interfejs extends Frame
{
    private FileHandler handler = null;
    private static Logger logger = Logger.getLogger("edu.java.lab6");

    private TextField textFieldRzymska;
    private TextField textFieldArabska;
    private Button buttonPrzycisk;
    private Panel panel;
    
    public Interfejs(){
        try {
            boolean append = true;
            FileHandler fh = new FileHandler("Konwerter.log", append);
//            fh.setFormatter(new SimpleFormatter());
            logger.addHandler(fh);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("Logger initialized");        

        this.setTitle("Konwersja liczb arabskich i rzymskich");
        this.setSize(200,180);

        textFieldRzymska = new TextField("",20);
        textFieldArabska = new TextField("",20);

        textFieldRzymska.addActionListener(textFieldRzymskaList);
        textFieldArabska.addActionListener(textFieldArabskaList);

        textFieldRzymska.addFocusListener(textFieldRzymskaFList);
        textFieldArabska.addFocusListener(textFieldArabskaFList);

        panel = new Panel();
        panel.setLayout(new FlowLayout() );
        panel.add(new Label("Liczba rzymska"));
        panel.add(textFieldRzymska);
        panel.add(new Label("Liczba arabska"));
        panel.add(textFieldArabska);

        this.add(panel);
        this.addWindowListener(frameList);

        logger.info("Panel created");

    }

    private WindowListener frameList = new WindowAdapter(){
        public void windowClosing(WindowEvent ev){
                logger.info("Main window is going to close.");
                Interfejs.this.dispose();
                System.exit(0);
        }
    };

    private ActionListener textFieldArabskaList = new ActionListener(){
        public void actionPerformed (ActionEvent ev){
            try{
                int liczba = Integer.parseInt(textFieldArabska.getText());
                String wynik = RzymArab.arab2rzym(liczba);
                textFieldRzymska.setText(wynik);
                logger.info("Changed "+liczba+" to: "+wynik);
            }
            catch (WyjatekKonwersji w){
                logger.warning("Bad arabic number given");
                dialogArabska d = new dialogArabska(Interfejs.this);
                d.show();
            }
        }
    };
    
    private ActionListener textFieldRzymskaList = new ActionListener(){
        public void actionPerformed (ActionEvent ev){
            try{
                String liczba = textFieldRzymska.getText();
                String wynik = String.valueOf(RzymArab.rzym2arab(liczba));
                textFieldArabska.setText(wynik);
                logger.info("Changed "+liczba+" to: "+wynik);
            }
            catch (WyjatekKonwersji w) {
                logger.warning("Bad roman number given");
                dialogRzymska d = new dialogRzymska(Interfejs.this);
                d.show();
            }
        }
    };

    private FocusListener textFieldRzymskaFList = new FocusListener(){
        public void focusGained(FocusEvent e){
            logger.info("Romant text field has gained focus.");
//            textFieldRzymska.setText("");
        }

        public void focusLost(FocusEvent e) {
            logger.info("Romant text field has lost focus.");
            textFieldRzymska.setText("");
        } 
    };
    
    private FocusListener textFieldArabskaFList = new FocusListener(){
        public void focusGained(FocusEvent e){
            logger.info("Arabic text field has gained focus.");
            //textFieldArabska.setText("");
        }
            
        public void focusLost(FocusEvent e) {
            logger.info("Arabic text field has lost focus.");
            textFieldArabska.setText("");
        }
    };
    
    private class dialogArabska extends Dialog{
        private WindowListener frameList = new WindowAdapter(){
            public void windowClosing(WindowEvent ev){
                dialogArabska.this.hide();
            } 
        };
        
        private ActionListener btnList = new ActionListener(){
            public void actionPerformed (ActionEvent ev){       
                dialogArabska.this.hide();
            }
        };
        
        
        public dialogArabska(Frame owner){
            super(owner, "Blad");
            this.setSize(500,100);
            this.setModal(true);
            this.addWindowListener(frameList);
            panel = new Panel();
            lbl = new Label("Podana liczba miesci się poza zakresem!");
            panel.add(lbl);
            btn = new Button("OK");
            panel.add(btn);
            btn.addActionListener(btnList);
            this.add(panel);

            panel.setLayout(new FlowLayout() );
        }

        private Panel panel;
        private Label lbl;
        private Button btn;
    }

    private class dialogRzymska extends Dialog{
        private WindowListener frameList = new WindowAdapter(){
            public void windowClosing(WindowEvent ev){
                dialogRzymska.this.hide();
            } 
        };
        
        private ActionListener btnList = new ActionListener(){
            public void actionPerformed (ActionEvent ev){       
                dialogRzymska.this.hide();
            }
        };
                
        public dialogRzymska(Frame owner){
            super(owner, "Blad");
            this.setSize(200,100);
            this.setModal(true);
            this.addWindowListener(frameList);
            panel = new Panel();
            lbl = new Label("Nieprawidłowy format liczby!");
            panel.add(lbl);
            btn = new Button("OK");
            panel.add(btn);
            btn.addActionListener(btnList);
            this.add(panel);

            panel.setLayout(new FlowLayout() );
        }

        private Panel panel;
        private Label lbl;
        private Button btn;
    }
}
